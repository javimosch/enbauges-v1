const Login = {
    template: '#login',
    mounted(){
    	this.$router.push('/admin/home')
    }
}

const Home = {
    template: '#home'
};

const ManagePagesContainer = {
    template: '#ManagePagesContainer',
    mounted(){
    	fetch('/api/pages').then(r=>r.json()).then(list=>{
    		console.log('LIST',{
    			list
    		})
    	})
    }
};

const routes = [
    { path: '/admin/login', component: Login },
    { path: '/admin/home', component: Home },
    { path: '/admin/pages', component: ManagePagesContainer }
]

const router = new VueRouter({
	mode: 'history',
    routes
});

new Vue({
    router
}).$mount('#app');
