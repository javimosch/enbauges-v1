module.exports = {
	middleware: (app, options) => {
		app.get('/bauges', (req, res) => {
			res.send('BAUGES IS ONLINE')
		});
		app.db = require('./db')(options);
	},
	runTests(options = {}){
		runTests(options).then(()=>{
			console.log('Tests passed')
		});
	}
}

async function runTests(options){

	var path = require('path')
	var db = require('./db')(options);

	await db.pages.getModel().deleteOne({
		name:'article-3'
	})

	//default features
	/*
	await db.features.saveOrUpdateByField({
		code: 'ARTICLES_LIST',
		name:'List of articles (default)',
		description:'Will display a list of articles',
		options:{
			hashtags:['']
		}
	}, 'code');
	*/

	await db.pages.saveOrUpdateByField({
		title: 'Home Page',
		path:'/index',
		html:'<h1>HOME THIS IS HOME!!</h1>'
	}, 'title');

	let compiler = require('./compiler')(options)

	await compiler.clearDistDirectory();
	await compiler.compilePages();

}
