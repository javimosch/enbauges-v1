module.exports = {
	create(modelName,options) {
		
		let {
			conn,
			middlewares,
			schema
		} = options;

		schema.pre('save', async function(next) {
			await middlewares.beforeSave(this);
			next();
		});

		let model = conn.model(modelName, schema);

		let scope = {
			async create(data) {
				console.log('create', {
					data
				})
				await middlewares.beforeSave(data);
				await model.create(data);
			},
			async saveOrUpdateByField(data, field) {
				let exists = await scope.existsByField(field, data[field])
				if (exists) {
					await scope.updateByField(field, data)
				} else {
					await scope.create(data)
				}
			},
			getModel(){
				return model;
			},
			async existsByField(field, value) {
				let condition = {
					[field]: value
				}
				len = await model.countDocuments(condition)
				return len > 0
			},
			async updateByField(field, data) {
				await middlewares.beforeSave(data);
				await model.updateOne({
					[field]: data[field]
				}, {
					$set: data
				});
			},
			async getAll() {
				return await model.find({}) //.select('title path html')
			}
		}
		return scope;
	}
}